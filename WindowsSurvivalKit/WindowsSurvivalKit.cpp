// WindowsSurvivalKit.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CreateShortCut.h"
#include "windows.h"
#include "winnls.h"
#include "shobjidl.h"
#include "objbase.h"
#include "objidl.h"
#include "shlguid.h"
#include "version.h"

#define APPDIR L"<appdir>"
#define APPDESCSEP string("::")

using namespace std;

void readApps(const wstring&, wstring startFolder, CreateShortCut&, const wstring&);
void readSettings(const wstring&, wstring& startMenuPath);

string ws2s(const std::wstring& wstr) {
	std::string str(wstr.begin(), wstr.end());
	return str;
}

wstring s2ws(const std::string& wstr) {
	std::wstring str(wstr.begin(), wstr.end());
	return str;
}

template<class T> void replace(T& str, const T& from, const T& to) {
	T::size_type tokenLoc = str.find(from);
	if (tokenLoc != T::npos) {
		str.replace(tokenLoc, from.length(), to);
	}
}

int main() {
	cout << "Windows Survival Kit" << endl;
	cout << "Build " << BUILD_ID << " (git " << BUILD_GIT_HASH << ")" << endl;
	cout << "Copyright 2016 Campbell Suter" << endl;

	CreateShortCut csc;

	wchar_t execFileName[MAX_PATH];
	GetModuleFileName(NULL, execFileName, MAX_PATH);

	wstring confDir = execFileName;
	confDir = confDir.substr(0, confDir.length() - 4); // trim off ".exe"

	wstring options = confDir + L"/options.txt";
	wstring comment = confDir; // L"From Windows Survival Kit"

	wstring startMenuDir = L"#Windows Survival Kit";
	readSettings(confDir, startMenuDir);
	readApps(confDir, startMenuDir, csc, comment);

	//Using with the newly introduced CSC_* constants - this example will create a shortcut on the current machine's desktop
	// csc.CreateLinkFileBase(L"C:\\Program Files", CSC_DESKTOP, comment.c_str(), NULL, L"");

	return 0;
}

void readSettings(const wstring& path, wstring& startMenuPath) {
	ifstream settings(path + L"/conf.ini");
	if (!settings.is_open()) return;
	string line;
	while (getline(settings, line)) {
		if (line.length() > 0) {
			string::size_type seperater = line.find("=");
			string key = line.substr(0, seperater);
			string value = line.substr(seperater + 1);
			cout << key << "=" << value << endl;

			wstring file = s2ws(value);
			replace<wstring>(file, APPDIR, path);
			LPWSTR lfile = const_cast<LPWSTR>(file.c_str());

			if (key == "background") {
				SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, lfile, 0); // SPIF_UPDATEINIFILE
			}
			else if (key == "startFolder") {
				startMenuPath = s2ws(value);
			}
		}
	}
	settings.close();
}

void readApps(const wstring& path, wstring startFolder, CreateShortCut& csc, const wstring& comment) {
	startFolder = L"\\" + startFolder;
	startFolder = CSC_STARTMENU + startFolder;
	cout << "Writing to " << ws2s(startFolder) << endl;
	LPCWSTR fileDest = startFolder.c_str();
	ifstream appLocList(path + L"/apps.txt");
	if (!appLocList.is_open()) return;
	string line;
	while (getline(appLocList, line)) {
		if (line.length() > 0) {
			wstring wline(line.begin(), line.end());

			wstring::size_type seperater = line.find(APPDESCSEP);
			wstring filename;
			wstring target;
			if (seperater == wstring::npos) {
				target = wline;
			}
			else {
				target = wline.substr(seperater + APPDESCSEP.length());
				filename = wline.substr(0, seperater);
				cout << ws2s(filename) << endl;
			}

			replace<wstring>(target, APPDIR, path);
			cout << "Linking to " << ws2s(target) << endl;
			csc.CreateLinkFileBase(target.c_str(), fileDest, comment.c_str(), NULL, L"", true, seperater == wstring::npos ? NULL : filename.c_str());
		}
	}
	appLocList.close();
}
